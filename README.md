[![Build Status](https://travis-ci.org/andreasonny83/sonny-portfolio.svg?branch=master)](https://travis-ci.org/andreasonny83/sonny-portfolio)
# SonnY portfolio
My web portfolio is running live at [SonnyWebDesign.com](http://sonnywebdesign.com/)

## install
To install the project with all the dependencies, git clone the project to your local machine, then run ``npm install`` and ``bower install`` from inside the project's root folder.
Then run ``gulp`` to compile the project in development mode and render the project directly into your browser.
